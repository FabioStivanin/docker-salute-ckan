
//MAIN FUNCYION CLASS DRAWCLOUD

function drawCluod(str, id){
            //var str='{{tags}}';
            str= replaceAll(str,"&#34;",'"');
            str= replaceAll(str,"&#39;",'');
            var frequency_list = JSON.parse(str);   

            minSize = 40; 
            for(var i = frequency_list.length - 1; i >= 0; i--) {
            if(frequency_list[i].size <= minSize) {
            frequency_list.splice(i, 1);
                }
            } 

        var fill = d3.scale.category20();
        //var fill= d3.scale.linear().domain(12,291).range(10,20);

        var color = d3.scale.linear()
                    .domain([0,1,2,3,4,5,6,10,15,20,100])
                    .range(["#ddd", "#ccc", "#bbb", "#aaa", "#999", "#888", "#777", "#666", "#555", "#444", "#333", "#222"]);

        function draw(words) {
            //var id= "#cloud"
            d3.select(id).append("svg") //where to insert the cloud
                        .attr("width", 1000)
                        .attr("height", 350)
            .append("g")
                .attr("transform", "translate(450,200)")
            .selectAll("text")
                .data(words)
            // .style("fill", function(d, i) { return fill(i); })
            .enter().append("text")
                .style("font-size", function(d) { return  d.size + "px"; })
                .style("font-family", "Impact")
                .style("fill", function(d, i) { return fill(i); })
                //.style("fill", function(d, i) { return color(i); }) //gray 
                .attr("text-anchor", "middle")
                .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function(d) { return d.text; });
        }


        d3.layout.cloud().size([800, 300])
            .words(frequency_list)
            .padding(5)
            .rotate(function(d) { return ~~(0.2* 2) * 90; })
            //    .font("Impact")
            .text(function(d) { return d.text; }) 
            .fontSize(function(d) { return  Math.sqrt(d.size*20); })
            .on("end", draw)
            .start();
}



// AUXILIARY
function escapeRegExp(str) {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        }
function replaceAll(str, find, replace) {
        return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
        }