import os
import ckan.lib.uploader as uploader
import cgi
import ckan.lib.munge as munge
import datetime
import ckan.logic as logic


from pylons import config

import logging
log = logging.getLogger(__name__)


class CustomResourceUpload(object):

	def __init__(self, resource):

		path = uploader.get_storage_path()
		if not path:
			self.storage_path = None
			return
		self.storage_path = os.path.join(path, 'resources')
		try:
			os.makedirs(self.storage_path)
		except OSError, e:
			# errno 17 is file already exists
			if e.errno != 17:
				raise
		self.filename = None

		url = resource.get('url')
		upload_field_storage = resource.pop('upload', None)
		self.clear = resource.pop('clear_upload', None)

		if isinstance(upload_field_storage, cgi.FieldStorage):
			self.filename = upload_field_storage.filename
			self.filename = munge.munge_filename(self.filename)
			resource['url'] = self.filename
			resource['url_type'] = 'upload'
			resource['last_modified'] = datetime.datetime.utcnow()
			self.upload_file = upload_field_storage.file
		elif self.clear:
			resource['url_type'] = ''

	def get_directory(self, id):
		directory = os.path.join(self.storage_path, id[0:3], id[3:6])
		return directory

	def get_path(self, id):
		directory = self.get_directory(id)
		filepath = os.path.join(directory, id[6:])
		return filepath

	# CUSTOMIZATION #####################
	def upload(self, id, max_size=10):
		if not self.storage_path:
			return

		directory = self.get_directory(id)
		filepath = self.get_path(id)

		if self.filename:
			try:
				os.makedirs(directory)
			except OSError, e:
				if e.errno != 17:
					raise
			tmp_filepath = filepath + '~'
			output_file = open(tmp_filepath, 'wb+')
			self.upload_file.seek(0)
			current_size = 0

			while True:

				current_size = current_size + 1
				data = self.upload_file.read(2 ** 20)
				if not data:
					break
				#try:
				# solo prima volta
				if current_size == 1:

					p, f_ext = os.path.splitext(self.filename)
					f_ext = f_ext.upper()
					ext_blacklist = config.get('ckan.dcatapit.upload_ext_blacklist', '').split() 

					log.info('trying to upload file with ext: '+ str(f_ext)) 

					#log.info(ext_blacklist)
					
					
					if f_ext in ext_blacklist:
						os.remove(tmp_filepath)
						raise logic.ValidationError(
							{'upload': ['Formato del file >> ' + str(f_ext)+' << illegale']}
						)
						break

					#log.info('\nMAGIC') #######
					
					import magic

					mimetype = magic.from_buffer(data, mime=True)
					mt_blacklist = config.get('ckan.dcatapit.upload_mimetype_blacklist', '').split() 

					log.info('trying to upload file with mimetype: '+ str(mimetype))
					
					if mimetype in mt_blacklist:
						os.remove(tmp_filepath)
						raise logic.ValidationError(
							{'upload': ['Formato del file >> ' + str(mimetype)+' << illegale']}
						)
						break
					
					#else:
					#	print 'Formato del file >> ' + str(mimetype)+' << accettato :-)'
				#except ImportError:
				#	log.info('\nno module magic\n')

				#log.info('\nWRITE') #######
				output_file.write(data)
				if current_size > max_size:
					os.remove(tmp_filepath)
					raise logic.ValidationError(
						{'upload': ['File upload too large']})

			#log.info('\nclose') #######
			output_file.close()
			os.rename(tmp_filepath, filepath)
			return

		if self.clear:
			try:
				os.remove(filepath)
			except OSError, e:
				pass	 