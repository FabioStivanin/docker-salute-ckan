import ckanext.dcatapit.interfaces as interfaces

from ckan.common import _, ungettext
from ckan.plugins import PluginImplementations


def get_custom_config_schema(show=True):
	if show:
		return [
		    {
			    'name': 'ckanext.dcatapit_configpublisher_name',
			    'validator': ['not_empty'],
			    'element': 'input',
			    'type': 'text',
			    'label': _('Dataset Editor'),
			    'placeholder': _('dataset editor'),
			    'description': _('The responsible organization of the catalog'),
			    'is_required': True
		    },
		    {
			    'name': 'ckanext.dcatapit_configpublisher_code_identifier',
			    'validator': ['not_empty'],
			    'element': 'input',
			    'type': 'text',
			    'label': _('Catalog Organization Code'),
			    'placeholder': _('IPA/IVA'),
			    'description': _('The IVA/IPA code of the catalog organization'),
			    'is_required': True
		    },
		    {
			    'name': 'ckanext.dcatapit_config.catalog_issued',
			    'validator': ['ignore_missing'],
			    'element': 'input',
			    'type': 'date',
			    'label': _('Catalog Release Date'),
			    'format': '%d-%m-%Y',
			    'placeholder': _('catalog release date'),
			    'description': _('The creation date of the catalog'),
			    'is_required': False
		    }
		]
	else:
		return [
		    {
			    'name': 'ckanext.dcatapit_configpublisher_name',
			    'validator': ['not_empty']
		    },
		    {
			    'name': 'ckanext.dcatapit_configpublisher_code_identifier',
			    'validator': ['not_empty']
		    },
		    {
			    'name': 'ckanext.dcatapit_config.catalog_issued',
			    'validator': ['ignore_missing']
		    }
		]

def get_custom_organization_schema():
	return [
	    {
		    'name': 'email',
		    'validator': ['ignore_missing'],
		    'element': 'input',
		    'type': 'email',
		    'label': _('EMail'),
		    'placeholder': _('organization email'),
		    'is_required': True
	    },
	    {
		    'name': 'telephone',
		    'validator': ['ignore_missing'],
		    'element': 'input',
		    'type': 'text',
		    'label': _('Telephone'),
		    'placeholder': _('organization telephone'),
		    'is_required': False
	    },
	    {
		    'name': 'site',
		    'validator': ['ignore_missing'],
		    'element': 'input',
		    'type': 'url',
		    'label': _('Site URL'),
		    'placeholder': _('organization site url'),
		    'is_required': False
	    }
	]

def get_custom_package_schema():
	package_schema = [
	    {
		    'name': 'identifier',
		    'validator': ['not_empty', 'dcatapit_id_unique'],
		    'element': 'input',
		    'type': 'text',
		    'label': _('Dataset Identifier'),
		    'placeholder': _('dataset identifier'),
		    'is_required': True,
			'ignore_from_info' : True,
		    'add_to_extra': True
	    },
	    {
		    'name': 'alternate_identifier',
		    'validator': ['ignore_missing', 'no_number'],
		    'element': 'input',
		    'type': 'text',
		    'label': _('Other Identifier'),
		    'placeholder': _('other identifier'),
		    'is_required': False,
			'ignore_from_info' : True,
		    'add_to_extra': True
	    },
	    {
		    'name': 'theme',
		    'validator': ['not_empty'],
		    'element': 'theme',
		    'type': 'vocabulary',
		    'vocabulary_name': 'eu_themes',
		    'label': _('Dataset Themes'),
		    'placeholder': _('eg. education, agriculture, energy'),
		    'data_module_source': '/api/2/util/vocabulary/autocomplete?vocabulary_id=eu_themes&incomplete=?',
		    'is_required': True,
			'ignore_from_info' : True,
		    'add_to_extra': True
	    },
	    {
		    'name': 'sub_theme',
		    'ignore':True,
		    'validator': ['ignore_missing'],
		    'element': 'theme',
		    'type': 'vocabulary',
		    'vocabulary_name': 'eurovoc',
		    'label': _('Sub Theme'),
		    'placeholder': _('sub theme of the dataset'),
			'data_module_source': '/api/2/util/vocabulary/autocomplete?vocabulary_id=eurovoc&incomplete=?',
		    'is_required': False,
		    'ignore_from_info' : True,
		    'add_to_extra': True
	    },
	    {
		    'name': 'publisher',
		    'element': 'couple',
		    'label': _('Dataset Editor'),
		    'is_required': True,
			'ignore_from_info' : True,
		    'couples': [
		    	{
		    		'name': 'publisher_name',
		    		'validator': ['not_empty'],
		    		'label': _('Name'),
		    		'type': 'text',
		    		'placeholder': _('publisher name'),
                	'localized': True
		    	},
			    {
		    		'name': 'publisher_identifier',
		    		'validator': ['not_empty'],
		    		'label': _('IPA/IVA'),
		    		'type': 'text',
		    		'placeholder': _('publisher identifier')
		    	}
		    ]
	    },
	    {
		    'name': 'issued',
		    'validator': ['ignore_missing'],
		    'element': 'input',
		    'type': 'date',
		    'label': _('Release Date'),
		    'format': '%d-%m-%Y',
		    'placeholder': _('release date'),
		    'is_required': False,
			'ignore_from_info' : True
	    },
	    {
		    'name': 'modified',
		    'validator': ['ignore_missing'],
		    'element': 'input',
		    'type': 'date',
		    'label': _('Modification Date'),
		    'format': '%d-%m-%Y',
		    'placeholder': _('modification date'),
		    'is_required': False,
			'ignore_from_info' : True
	    },
	    {
		    'name': 'geographical_name',
		    'validator': ['ignore_missing'],
		    'element': 'theme',
		    'type': 'vocabulary',
		    'vocabulary_name': 'places',
		    'label': _('Geographical Name'),
		    'placeholder': _('geographical name'),
		    'data_module_source': '/api/2/util/vocabulary/autocomplete?vocabulary_id=places&incomplete=?',
		    'is_required': False,
		    'default': _('Organizational Unit Responsible Competence Area'),
			'add_to_extra': True
	    },
	    {
		    'name': 'geographical_geonames_url',
		    'validator': ['ignore_missing'],
		    'element': 'input',
		    'type': 'url',
		    'label': _('GeoNames URL'),
		    'placeholder': _('http://www.geonames.org/3175395'),
		    'is_required': False,
			'add_to_extra': True
	    },
	    {
		    'name': 'language',
		    'validator': ['ignore_missing'],
		    'element': 'theme',
		    'type': 'vocabulary',
		    'vocabulary_name': 'languages',
		    'label': _('Dataset Languages'),
		    'placeholder': _('eg. italian, german, english'),
		    'data_module_source': '/api/2/util/vocabulary/autocomplete?vocabulary_id=languages&incomplete=?',
		    'is_required': False,
			'add_to_extra': True
	    },
	    {
		    'name': 'temporal_coverage',
		    'element': 'couple',
		    'label': _('Temporal Coverage'),
		    'is_required': False,
		    'couples': [
		    	{
		    		'name': 'temporal_start',
		    		'label': _('Start Date'),
		    		'validator': ['ignore_missing'],
		    		'type': 'date',
		    		'format': '%d-%m-%Y',
		    		'placeholder': _('temporal coverage')
		    	},
			    {
		    		'name': 'temporal_end',
		    		'label': _('End Date'),
		    		'validator': ['ignore_missing'],
		    		'type': 'date',
		    		'format': '%d-%m-%Y',
		    		'placeholder': _('temporal coverage')
		    	}
		    ]
	    },
	    {
		    'name': 'frequency',
		    'validator': ['not_empty'],
		    'element': 'select',
		    'type': 'vocabulary',
		    'vocabulary_name': 'frequencies',
		    'label': _('Frequency'),
		    'placeholder': _('accrual periodicity'),
		    'data_module_source': '/api/2/util/vocabulary/autocomplete?vocabulary_id=frequencies&incomplete=?',
		    'is_required': True,
			'add_to_extra': True
	    },
	    {
		    'name': 'conforms_to',
		    'validator': ['ignore_missing', 'no_number'],
		    'element': 'input',
		    'type': 'text',
		    'label': _('Conforms To'),
		    'placeholder': _('conforms to'),
		    'is_required': False,
			'hide_if_absent' : True,
			'ignore_from_info' : True
	    },
	    {
		    'name': 'rights_holder',
		    'element': 'couple',
		    'label': _('Rights Holder'),
		    'is_required': True,
		    'couples': [
		    	{
		    		'name': 'holder_name',
		    		'label': _('Name'),
		    		'validator': ['not_empty'],
		    		'type': 'text',
		    		'placeholder': _('rights holder of the dataset'),
		    		'localized': True

		    	},
			    {
		    		'name': 'holder_identifier',
		    		'label': _('IPA/IVA'),
		    		'validator': ['not_empty'],
		    		'type': 'text',
		    		'placeholder': _('rights holder of the dataset')
		    	}
		    ]
	    },
	    {
		    'name': 'creator',
		    'element': 'couple',
		    'label': _('Creator'),
		    'is_required': False,
			'add_to_extra': True,
		    'couples': [
		    	{
		    		'name': 'creator_name',
		    		'label': _('Name'),
		    		'validator': ['ignore_missing'],
		    		'type': 'text',
		    		'placeholder': _('creator of the dataset'),
		    		'localized': True
		    	},
			    {
		    		'name': 'creator_identifier',
		    		'label': _('IPA/IVA'),
		    		'validator': ['ignore_missing'],
		    		'type': 'text',
		    		'placeholder': _('creator of the dataset')
		    	}
		    ]
	    },
		{
				'name': 'ico_indirizzo_struttura_riferimento',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_indirizzo_struttura_riferimento'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True
		},
		{
				'name': 'ico_email_struttura_riferimento',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_email_struttura_riferimento'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True
		},
		{
				'name': 'ico_email_ente_responsabile',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_email_ente_responsabile'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True
		},
		{
				'name': 'ico_HomePage_Ente_Responsabile',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_HomePage_Ente_Responsabile'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True
		},
		{
				'name': 'ico_tipo_ente_responsbile',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_tipo_ente_responsbile'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True
		},
		{
				'name': 'data_di_creazione_dato',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('data_di_creazione_dato'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True
		},
		{
				'name': 'ico_indirizzo_creatore',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_indirizzo_creatore'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True,
				'add_to_extra' : True

		},
		{
				'name': 'ico_richiesto_account',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_richiesto_account'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True,
				'add_to_extra' : True

		},
		{
				'name': 'ico_riferimenti_normativi',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_riferimenti_normativi'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True,
				'add_to_extra' : True

		},
		{
				'name': 'ico_fornitore_applicazione',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_fornitore_applicazione'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True,
				'add_to_extra' : True

		},
		{
				'name': 'ico_applicazione_produce_banca_dati',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_applicazione_produce_banca_dati'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True,
				'add_to_extra' : True

		},
		{
				'name': 'ico_applicazione_utilizza_banca_dati',
				'validator': ['ignore_missing'],
				'element': 'input',
				'type': 'text',
				'label': _('ico_applicazione_utilizza_banca_dati'.title().replace('Ico_', '').replace('_', ' ')),
				'placeholder': _(' '),
				'is_required': False,
				'localized': False,
				'hide_if_absent' : True,
				'add_to_extra' : True

		},
		{
				'name': 'mobilita_passiva',
				'validator': ['default_si', 'ignore_missing'],
				'element': 'select',
				'options': [{'value':'SI'}, {'value':'NO'}],
				'label': _('Mobilita\' Passiva'),
				'placeholder': _('SI o NO'),
				'is_required': False,
				'localized': False,
				'add_to_extra' : True

		}
		
		
		
		
	]

	for plugin in PluginImplementations(interfaces.ICustomSchema):
		extra_schema = plugin.get_custom_schema()

		for extra in extra_schema:
			extra['external'] = True

		package_schema = package_schema + extra_schema

	return package_schema

def get_custom_resource_schema():
	return [
 		{
		    'name': 'distribution_format',
		    'validator': ['ignore_missing'],
		    'element': 'select',
		    'type': 'vocabulary',
		    'vocabulary_name': 'filetype',
		    'label': _('Distribution Format'),
		    'placeholder': _('distribution format'),
		    'data_module_source': '/api/2/util/vocabulary/autocomplete?vocabulary_id=filetype&incomplete=?',
		    'is_required': False
	    }
	]
