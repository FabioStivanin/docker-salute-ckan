# encoding: utf-8
import os
import ckan.plugins as p
import ckan.plugins.toolkit as tk

#############################
# visualizzazione delle date
#############################
def custom_date(date, output_format="%d/%m/%Y"):
    from datetime import datetime
    from dateutil import parser
        
    try:
        parsed_date = parser.parse(date)
        return parsed_date.strftime(output_format)
    except:
        return 'N/A'


####################################
# VOCABOLARI
####################################

def create_ico_vocabolario_EnCos():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'ico_vocabolario_EnCos'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'ico_vocabolario_EnCos'}
        vocab = tk.get_action('vocabulary_create')(context, data)
        
        for tag in (u'Assente',u'Ministero Ambiente',u'Agenzia Demanio',u'Agenzia Regionale Protezione Civile',u'Agenzie TPL',u'AIPO',u'Anas',u'ARNI',u'ARPA',u'ASPI',u'ATO',u'AUSL',u'Autorita Bacino',u'Attivita Vigilanza Lavori Pubblici',u'Comuni',u'Consorzio Bonifica',u'Enti Locali',u'Enti Parco',u'Enti Territoriali',u'Esercenti',u'Fer',u'Gestori Autostradali',u'Servizio Gestione Rifiuti Urbani',u'INAIL',u'INPS',u'MB',u'MIBACT',u'Ministeri',u'Prefettura',u'Province',u'Regione Emilia-Romagna',u'Servizio Difesa SUolo-Costa-Bonifica',u'Seta',u'Sovrintendenze',u'StackHolders',u'Tper',u'Trenitalia'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def ico_vocabolario_EnCos():
    create_ico_vocabolario_EnCos()
    try:
        tag_list = tk.get_action('tag_list')
        ico_vocabolario_EnCos = tag_list(data_dict={'vocabulary_id': 'ico_vocabolario_EnCos'})
        return ico_vocabolario_EnCos
    except tk.ObjectNotFound:
        return None



def create_ico_vocabolario_LPs():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'ico_vocabolario_LPs'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'ico_vocabolario_LPs'}
        vocab = tk.get_action('vocabulary_create')(context, data)
        
        for tag in (u'Assente',u'Pubblicato',u'Non Pubblicato',u'In parte pubblicato',u'Pubblicabile',u'Non Pubblicabile',u'Uso Interno'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def ico_vocabolario_LPs():
    create_ico_vocabolario_LPs()
    try:
        tag_list = tk.get_action('tag_list')
        ico_vocabolario_LPs = tag_list(data_dict={'vocabulary_id': 'ico_vocabolario_LPs'})
        return ico_vocabolario_LPs
    except tk.ObjectNotFound:
        return None

def create_ico_vocabolario_PPs():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'ico_vocabolario_PPs'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'ico_vocabolario_PPs'}
        vocab = tk.get_action('vocabulary_create')(context, data)
        
        for tag in (u'Assente',u'Geoportale',u'Sito FER',u'Opendata',u'Portale RER'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def ico_vocabolario_PPs():
    create_ico_vocabolario_PPs()
    try:
        tag_list = tk.get_action('tag_list')
        ico_vocabolario_PPs = tag_list(data_dict={'vocabulary_id': 'ico_vocabolario_PPs'})
        return ico_vocabolario_PPs
    except tk.ObjectNotFound:
        return None

def create_ico_vocabolario_attivitas():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'ico_vocabolario_attivitas'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'ico_vocabolario_attivitas'}
        vocab = tk.get_action('vocabulary_create')(context, data)
        
        for tag in (u'Monitoraggio',u'Permessi',u'Pianificazione',u'Non Richiesto'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def ico_vocabolario_attivitas():
    create_ico_vocabolario_attivitas()
    try:
        tag_list = tk.get_action('tag_list')
        ico_vocabolario_attivitas = tag_list(data_dict={'vocabulary_id': 'ico_vocabolario_attivitas'})
        return ico_vocabolario_attivitas
    except tk.ObjectNotFound:
        return None
        
def create_ico_vocabolario_dato_certificatos():
    user = tk.get_action('get_site_user')({'ignore_auth': True}, {})
    context = {'user': user['name']}
    try:
        data = {'id': 'ico_vocabolario_dato_certificatos'}
        tk.get_action('vocabulary_show')(context, data)
    except tk.ObjectNotFound:
        data = {'name': 'ico_vocabolario_dato_certificatos'}
        vocab = tk.get_action('vocabulary_create')(context, data)
        
        for tag in (u'Assente',u'Non Certificato',u'Certificato'):
            data = {'name': tag, 'vocabulary_id': vocab['id']}
            tk.get_action('tag_create')(context, data)

def ico_vocabolario_dato_certificatos():
    create_ico_vocabolario_dato_certificatos()
    try:
        tag_list = tk.get_action('tag_list')
        ico_vocabolario_dato_certificatos = tag_list(data_dict={'vocabulary_id': 'ico_vocabolario_dato_certificatos'})
        return ico_vocabolario_dato_certificatos
    except tk.ObjectNotFound:
        return None        
