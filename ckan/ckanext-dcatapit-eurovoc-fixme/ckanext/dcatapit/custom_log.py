import logging
from ckan.common import c
from ckan.lib.helpers import get_action
import ckan.model as model
import requests

import pylons
from pylons import request

class CustomFormatter(logging.Formatter):
	def __init__(self, default):
		self.default = default

	def format(self, record):

		#log = logging.getLogger(__name__)
		user=""
		ip =""
		try:
			if pylons.c.user:
				user= "USER="+str(pylons.c.user)
			else: 
				user= "USER=NotLogged"
			
			#log.info(request.headers)
			if request.headers.get('X-Forwarded-For'):
				ip = "IP="+str(request.headers.get('X-Forwarded-For'))
			elif request.headers.get('http_RERFWFOR'): 
				ip = "IP="+str(request.headers.get('http_RERFWFOR'))
			
		except:
			pass

		record.custom_ip = ip
		record.user = user

		return self.default.format(record)

def factory(fmt, datefmt):
	default = logging.Formatter(fmt, datefmt)
	return CustomFormatter(default)